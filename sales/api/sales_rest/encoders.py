from common.json import ModelEncoder
from .models import Sale, Salesperson, Customer, AutomobileVO


class SalespeopleListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]


class CustomersListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile",
    ]

    encoders = {
        "salesperson": SalespeopleListEncoder(),
        "customer": CustomersListEncoder(),
        "automobile": AutomobileVODetailEncoder(),
    }
