import React, { useEffect, useState } from 'react';


function ServiceHistory() {
    const [vin, setVin] = useState("");
    const [appointments, setPastAppointments] = useState([]);


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }


    const vinSearch = async (vin) => {
        const result = appointments.filter(appointment => {
            return appointment.vin.includes(vin);
        });
        setPastAppointments(result);
        };


    async function loadPastAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            const result = data.appointments.filter(appointment => appointment.status == 'cancelled' || appointment.status == 'finished')
            setPastAppointments(result);
        } else {
            console.error(response);
        };
    };


    useEffect(() => {
        loadPastAppointments();
    }, []);


    return (
        <>
        <h1>Service History</h1>
        <div className="input-group">
        <input value={vin} onChange={handleVinChange} type="search" className="form-control rounded" placeholder="Search by VIN" aria-label="Search" aria-describedby="search-addon" />
        <button onClick={() => vinSearch(vin)} type="button" className="btn btn-outline-primary">search</button>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                    <tr key={appointment.id}>
                        <td>{ appointment.vin }</td>
                        <td>{ String(appointment.is_vip) }</td>
                        <td>{ appointment.customer }</td>
                        <td>{ appointment.date }</td>
                        <td>{ appointment.time }</td>
                        <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                        <td>{ appointment.reason }</td>
                        <td>{ appointment.status }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default ServiceHistory;
