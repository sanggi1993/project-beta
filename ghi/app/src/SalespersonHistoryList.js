import React, { useEffect, useState } from 'react';


function SalespersonHistoryList() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalesPeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('')


    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        } else {
            console.error(response);
        }
    }


    const fetchSalespeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;

        setSelectedSalesperson(value);
    }


    useEffect(() => {
        loadSales();
        fetchSalespeopleData();
    }, []);

    return (
        <div>
            <h1>Salesperson History</h1>
            <div className="mb-3">
                <select value={selectedSalesperson}
                    onChange={handleFormChange}
                    required name="salesperson"
                    id="salesperson" className="form-select"
                >
                <option value="">Choose a salesperson</option>
                {salespeople.map(salesperson => {
                return (
                    <option
                        key={salesperson.id} value={salesperson.id}
                    >
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                )
                })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter(sale => sale.salesperson.id == selectedSalesperson).map(sale => {
                        return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.first_name } {sale.salesperson.last_name }</td>
                            <td>{ sale.customer.first_name } {sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>{ sale.price }</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonHistoryList;
