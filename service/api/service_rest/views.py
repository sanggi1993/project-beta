from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianEncoder, AppointmentEncoder
from .models import Technician, Appointment, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
        except content.DoesNotExist:
            return JsonResponse(
                {"message": "technician could not be created"},
                status=400,
            )
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_technicians(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.get(id=id).delete()
            return JsonResponse({"technician deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "technician does not exist"},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            if AutomobileVO.objects.filter(vin=content["vin"]).exists():
                content["is_vip"] = True
            else:
                pass
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "appointment could not be created"}
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_appointments(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.get(id=id).delete()
            return JsonResponse({"deleted appointment": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "appointment does not exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_cancel_appointments(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.cancel()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "appointment does not exist"},
                status=400,
            )
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_finish_appointments(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.finish()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "appointment does not exist"},
                status=400,
            )
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )
