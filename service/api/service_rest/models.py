from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    sold = models.BooleanField(null=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField(unique=True)


class Appointment(models.Model):
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.CharField(max_length=200)
    customer = models.CharField(max_length=100)
    is_vip = models.BooleanField(null=True, default=False)
    vin = models.CharField(max_length=100)
    status = models.CharField(default="scheduled", max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def cancel(self):
        self.status = "cancelled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()
