# CarCar

Team:

* Jisun Lee - Service
* Jonah Han - Sales

## Design

## Service microservice

For the service microservice, there will be a technician model to handle the details of each technician,
an appointment model to handle the details of appointments,
and an automobileVO model which will contain information on car vin number and whether or not it was sold.

The automobileVO is connected to the inventory service.
Poller will be used to update the automobielVO every 60 seconds with updated vins.

## Sales microservice

There will be three models that I will have to create with the Sales microservice, which are
1. Salesperson model with their names and employee id
2. Customer model with their names, address and phone number
3. Sales model with most of the fields being a foreign key like automobile, salesperson, and customer. There will also be a field that isn't a foreign key which is the price

4. Lastly we wiil have AutomobileVO model that we will poll from the inventory microservice to get the automobile's vin and sold fields.
